
import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
import torchvision
import matplotlib.pyplot as plt
import torchvision.models as models
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils, datasets
from torchvision.transforms import Normalize
import os
from tensorboardX import SummaryWriter
import numpy as np
import copy
import time

class_names = ['colgate', 'skipper', 'kellogs', ' scotti', 'tonnorio', 'valfrutta', 'mezze', 'sale']
data_dir = '/home/lua/pytorch-classification/data/custom'
model_path = ''

testdir = os.path.join(data_dir, 'test')

use_gpu = torch.cuda.is_available()

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])

test_loader = torch.utils.data.DataLoader(
    datasets.ImageFolder(testdir, transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        normalize,
    ])),
    batch_size=8, shuffle=True)


net = models.resnet50(pretrained=True)
num_ftrs = net.fc.in_features
net.fc = nn.Linear(num_ftrs, 8)
net_dict = net.cuda().state_dict()
pretrained_dict = torch.load(model_path)['state_dict']
net_dict.update(pretrained_dict)
net.load_state_dict(net_dict)

def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(5)

def visualize_model(model, df, num_images=6):

    for i, data in enumerate(df):
        inputs, labels = data
        if use_gpu:
            inputs, labels = Variable(inputs.cuda()), Variable(labels.cuda())
            print (inputs, labels)
        else:
            inputs, labels = Variable(inputs), Variable(labels)

        outputs = model(inputs)
        _, preds = torch.max(outputs.data, 1)
        print preds
        for j in range(inputs.size()[0]):
            title = 'predicted: ' + class_names[preds[j]]
            imshow(inputs.cpu().data[j], title)


visualize_model(net, test_loader)
